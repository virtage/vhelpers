h1. Virtage Vhelpers - just another common utilities and snippets library

h2. Where's documentation?

Vhelpers common utilities library documentation is maintained only as javadoc. We believe this is only natural way for Java library as Vhelper.

Vhelpers is developed under commercial-friendly "Eclipse Public Licence":http://www.eclipse.org/legal/epl-v10.html.

h2. Installing as local Maven artifact

To be able to add VHelpers as dependency from other projects, you need to firstly build & install project on your local PC:

pre. $ mvn install

h2. Contributting to Vhelpers

Would you like to add some pretty handful helper method? You're welcome to fork Vhelpers and commit back using pull requests. Or for old schoolers, send us your patch.