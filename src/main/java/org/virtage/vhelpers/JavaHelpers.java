/*******************************************************************************
 * Copyright (c) 2013 Virtage Software, Libor Jelinek.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Virtage Software, Libor Jelinek - initial API and implementation
 *******************************************************************************/
package org.virtage.vhelpers;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Java language helpers
 * 
 * @author libor
 *
 */
public final class JavaHelpers {
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final Logger SYSLOG = LoggerFactory.getLogger(ThisClass.class);

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------
	
	private JavaHelpers() {}

	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------
	
	/**
	 * Converts exception stack trace to single string, every step on new line.
	 * 
	 * @return Stack trace as string or empty string ("") for null argument.
	 **/
	public static String stackTraceToString(Throwable t) {
		if (t == null) return "";
		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);				
		return sw.toString();
	}

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
}
