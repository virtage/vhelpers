/*******************************************************************************
 * Copyright (c) 2013 Virtage Software, Libor Jelinek.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Virtage Software, Libor Jelinek - initial API and implementation
 *******************************************************************************/
package org.virtage.vhelpers;

import java.text.Normalizer;

/**
 * @author libor
 *
 */
public final class StringHelpers {
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final Logger SYSLOG = LoggerFactory.getLogger(ThisClass.class);

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------
	
	private StringHelpers() {}		// Utility class is never instantiated

	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------
	
	/**
	 * "Un-accents" char, i.e. convert accented char to unaccented ones.
	 * 
	 * @param original
	 * @param replaceIllegalCharsWith
	 * @return Unaccented original, empty string for null original. 
	 */
	public static String unAccent(String original) {
		if (original == null)
			return "";
		
		// Some suggest to use {InCombiningDiacriticalMarks} instead of {IsM}
		return Normalizer.normalize(original, Normalizer.Form.NFD).replaceAll(
				"\\p{IsM}+", "");
	}

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
}
