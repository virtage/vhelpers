/*******************************************************************************
 * Copyright (c) 2013 Virtage Software, Libor Jelinek.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Virtage Software, Libor Jelinek - initial API and implementation
 *******************************************************************************/
package org.virtage.vhelpers;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

/**
 * Collections and maps (JDK 1.4+) framework helpers, obsolete {@link Enumeration}
 * and {@link Dictionary} framework helpers.
 * 
 * @author libor
 *
 */
public final class CollectionHelpers {
	
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final Logger SYSLOG = LoggerFactory.getLogger(ThisClass.class);

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------
	
	private CollectionHelpers() {}

	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------
	
	
	
	/**
	 * Converts obsolete Dictionary object to Map. 
	 *  
	 * @param source
	 * @param target
	 * @return
	 * @author Original code is authored by <a href="http://stackoverflow.com/users/1523648/oberlies">Oberlies</a>
	 * from <a href="http://stackoverflow.com/a/13141726/915931">this</a>
	 * StackOverflow answer.
	 */
	public static <K, V> Map<K, V> dictionaryToMap(Dictionary<K, V> source, Map<K, V> target) {
		// Argument checks
		if ((source == null) && (target == null)) {
			throw new NullPointerException("Both source and sink arguments are null.");
		}
		if (source == null) {
			throw new NullPointerException("Source argument is null.");
		}
		if (target == null) {
			throw new NullPointerException("Target argument is null.");
		}
		
		// Conversion
		for (Enumeration<K> keys = source.keys(); keys.hasMoreElements();) {
			K key = keys.nextElement();
			target.put(key, source.get(key));
		}
		
		return target;
	}

	/**
	 * Converts obsolete {@link Dictionary} objects to human readable string.
	 * 
	 * <p>
	 * For example 
	 * <pre>{@code
Dictionary<String, String> dict = new Hashtable<>();
dict.put("favoriteSong", "Hello Dolly");
	 * }</pre> 
	 * </p>
	 * 
	 * <p>
	 * Will output <code>{favoriteSong=Hello Dolly}</code>.
	 * </p>
	 * 
	 * @param dict Any object implementing {@link Dictionary} like {@link Hashtable}
	 * or {@link Properties}.
	 *  
	 * @return Human readable representation of argument. For null argument
	 *         return empty string, for empty dictionary returns <t>{}</tt>.
	 */
	public static String dictionaryToString(Dictionary<?, ?> dict) {
		if (dict == null) return "";
		
		StringBuilder sb = new StringBuilder();
		for (Object key : Collections.list(dict.keys())) {
			sb.append(", ");
			sb.append(key);
			sb.append("=");
			sb.append(dict.get(key));
		}
		// If dictionary wasn't empty it sb now looks like
		//		", prop1 = val1, prop2 = val2"
		// we need to remove leading ", "
		if (sb.length() > 0) {
			return "{" + sb.substring(2) + "}";
		}
		
		return "{" + sb.toString() + "}";
	}
	
	
	/**
	 * Converts {@link Map} objects to human readable string.
	 * 
	 * <p>
	 * For example 
	 * <pre>{@code
Map<String, String> map = new HashMap<>();
map.put("favoriteSong", "Hello Dolly");
	 * }</pre> 
	 * </p>
	 * 
	 * <p>
	 * Will output <code>{favoriteSong=Hello Dolly}</code>.
	 * </p>
	 *  
	 * @return Human readable representation of argument. For null argument
	 *         return empty string, for empty maps returns <t>{}</tt>.
	 */
	public static <K, V> String mapToString(Map<K, V> map) {
		if (map == null) return "";
		
		StringBuilder sb = new StringBuilder();
		for (K key : map.keySet()) {
			sb.append(", ");
			sb.append(key);
			sb.append("=");
			sb.append(map.get(key));
		}
		// If dictionary wasn't empty it sb now looks like
		//		", prop1 = val1, prop2 = val2"
		// we need to remove leading ", "
		if (sb.length() > 0) {
			return "{" + sb.substring(2) + "}";
		}
		
		return "{" + sb.toString() + "}";
	}

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
}
