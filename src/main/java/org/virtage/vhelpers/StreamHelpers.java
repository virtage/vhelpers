/*******************************************************************************
 * Copyright (c) 2013 Virtage Software, Libor Jelinek.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Virtage Software, Libor Jelinek - initial API and implementation
 *******************************************************************************/
package org.virtage.vhelpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * Various auxiliary file Java I/O methods. Class contains only static methods.
 * 
 * @author libor
 *
 */
public final class StreamHelpers {
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	/**
	 * Empirical determined optimal buffer size (value {@value}.)
	 */
	public static int BUFFER_SIZE = 8 * 1024;		// 'cause NIO.2 uses 8 KiB
	
	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	private StreamHelpers() {}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------
	
	/**
	 * Converts input stream to string using given charset.
	 * 
	 * @param is InputStream. Closing stream is client's
	 * @param cs Charset to use for conversion from stream to string. If null then platform default is used. 
	 * @return String or empty string ("") if input stream was null or empty. Never returns null. 
	 * @throws IOException re-thrown IOException 
	 */
	public static String streamToString(InputStream is, Charset cs) throws IOException {
		if (is == null)
			return "";
		
		StringBuilder sb = new StringBuilder();
		String line;
		
		InputStreamReader isr;
		if (cs != null) {
			isr = new InputStreamReader(is, cs);
		} else {
			isr = new InputStreamReader(is);
		}
		
		try (BufferedReader br = new BufferedReader(isr)) {
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		}
		
		return sb.toString();
	}
	
	/**
	 * Converts input stream to string using platform default charset.
	 * This is convenience method, identical to {@code streamToString(InputStream, null)}.
	 * 
	 * @param is
	 * @return String or empty string ("") if input stream was null or empty. Never returns null.
	 * @throws IOException re-thrown IOException 
	 */
	public static String streamToString(InputStream is) throws IOException {
		return streamToString(is, null);
	}

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
}
