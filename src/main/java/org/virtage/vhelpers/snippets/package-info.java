/*******************************************************************************
 * Copyright (c) 2013 Virtage Software, Libor Jelinek.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Virtage Software, Libor Jelinek - initial API and implementation
 *******************************************************************************/
/**
 * Example collection suite for learning purposes not developed by Virtage.
 * Every example can have own license.
 * 
 * <p>Examples contains header in format
 * <pre>
 * Source: http://.... 
 * Author: ...
 * License: ...
 * </pre>
 * </p>
 */
package org.virtage.vhelpers.snippets;