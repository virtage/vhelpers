/*
 * Source: http://javapapers.com/core-java/java-timer/ 
 * Author: Joseph Kulandai (Joe)
 * License: ...
 */

package org.virtage.vhelpers.snippets;
import java.util.Timer;
import java.util.TimerTask;

public class JdkTimer {

	public static void main(String args[]) {
		new TimerPiano(2, 4);
	}

	public static class TimerPiano {
		Timer dingTimer;
		Timer dongTimer;

		public TimerPiano(int ding, int dong) {

			dingTimer = new Timer();
			dingTimer.schedule(new Sound("Ding!"), ding * 1000, ding * 1000);
			// dingTimer.scheduleAtFixedRate(...);

			dongTimer = new Timer();
			dongTimer.schedule(new Sound("Dong!"), dong * 1000, dong * 1000);
			// dongTimer.scheduleAtFixedRate(...);
		}
	}

	public static class Sound extends TimerTask {
		//Toolkit toolkit;
		String sound;

		public Sound(String sound) {
			this.sound = sound;
			//toolkit = Toolkit.getDefaultToolkit();
		}

		@Override
		public void run() {
			System.out.println(sound);
			//toolkit.beep();
		}
	}

}
