/*******************************************************************************
 * Copyright (c) 2013 Virtage Software, Libor Jelinek.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Virtage Software, Libor Jelinek - initial API and implementation
 *******************************************************************************/
package org.virtage.vhelpers;

public final class PreconditionHelpers {

	// --------------------------------------------------------------------------
	// Class fields - public
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Class fields - non-public
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Instance fields - public
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Instance fields - non-public
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Instance fields - bean properties
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Constructors, static initializers, factories
	// --------------------------------------------------------------------------

	private PreconditionHelpers() {
	}

	// --------------------------------------------------------------------------
	// Public API - class (static)
	// --------------------------------------------------------------------------

	/**
	 * <p>
	 * Checks type of Object instance and cast to expected type if it's safe,
	 * otherwise throws an exception.
	 * </p>
	 * 
	 * <p>
	 * This method is ideal for JFace viewers accepting Object but your
	 * implementation must over and over cast to actual value objects. Example
	 * for content provider processing <code>java.nio.file.Path</code>
	 * instances:
	 * 
	 * <pre>
	 * &#064;Override
	 * public Object[] getElements(Object inputElement) {
	 * 	Path input = PreconditionHelpers.checkAndCast(inputElement, Path.class);
	 * }
	 * </pre>
	 * 
	 * </p>
	 * 
	 * 
	 * @param object
	 *            Object assumed to be of expected type
	 * @param expected
	 *            Expected type of object.
	 * @return Object casted to expected type.
	 * @throws IllegalArgumentException
	 *             if object type mismatch from expected.
	 * @throws NullPointerException
	 *             for null object or expected argument.
	 */
	public static <T> T checkAndCast(Object object, Class<T> expected)
			throws IllegalArgumentException {

		if (object == null)
			throw new NullPointerException("Object argument was null.");
		if (expected == null)
			throw new NullPointerException("Expected argument was null.");

		if (expected.isAssignableFrom(object.getClass()))
			return (T) object;

		// If reached this, throw IAE
		String msg = "Expected instance of " + expected.getName() + " but got "
				+ object.getClass().getName() + ".";
		throw new IllegalArgumentException(msg);
	}

	// --------------------------------------------------------------------------
	// Public API - instance
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Overrides and implementations
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Getters/setters of properties
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Helper methods (mostly private)
	// --------------------------------------------------------------------------
}
