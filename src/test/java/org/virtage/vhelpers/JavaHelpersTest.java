/*******************************************************************************
 * Copyright (c) 2013 Virtage Software, Libor Jelinek.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Virtage Software, Libor Jelinek - initial API and implementation
 *******************************************************************************/
package org.virtage.vhelpers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

import org.junit.Test;
import org.virtage.vhelpers.JavaHelpers;

/**
 * @author libor
 * 
 */
public class JavaHelpersTest {

	@Test
	public void stackTraceToString() {
		RuntimeException ex = new RuntimeException("hello");
		assertThat(JavaHelpers.stackTraceToString(ex),
				containsString("java.lang.RuntimeException: hello"));
	}

	@Test
	public void stackTraceToStringNullArgument() {
		assertEquals("", JavaHelpers.stackTraceToString(null));
	}
}
