/*******************************************************************************
 * Copyright (c) 2013 Virtage Software, Libor Jelinek.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Virtage Software, Libor Jelinek - initial API and implementation
 *******************************************************************************/
package org.virtage.vhelpers;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class PreconditionHelpersTest {

	@Test(expected=IllegalArgumentException.class)
	public void checkAndCastcheckAndCastMismathingArguments() {
		PreconditionHelpers.checkAndCast("hello", Double.class);
	}
	
	@Test
	public void checkAndCastSubclasses() {
		PreconditionHelpers.checkAndCast(new ArrayList<String>(), List.class);
	}

	@Test(expected=NullPointerException.class)
	public void checkAndCastNullArgument1() {
		PreconditionHelpers.checkAndCast(null, Double.class);
	}
	
	@Test(expected=NullPointerException.class)
	public void checkAndCastNullArgument2() {
		PreconditionHelpers.checkAndCast("hello", null);
	}
}
