package org.virtage.vhelpers;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.junit.Test;
import org.virtage.vhelpers.StreamHelpers;

public class StreamHelpersTest {

	@Test
	public void testStreamNullInputStream() throws IOException {
		assertEquals("", StreamHelpers.streamToString(null));
	}
	
	@Test
	public void streamToStringDifferentInputAndOutputStringTest() throws IOException {
		String sampleText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
		
		// Pass in-memory stream for better performance
		try (ByteArrayInputStream bais = new ByteArrayInputStream(
				sampleText.getBytes())) {
			assertEquals(sampleText, StreamHelpers.streamToString(bais));
		}
	}
	
	@Test
	public void streamToStringEmptyStreamReturnsEmptyStringNotNull() throws Exception {
		try (ByteArrayInputStream bais = new ByteArrayInputStream(new byte[] {})) {
			assertEquals("", StreamHelpers.streamToString(bais));
		}
	}
}
