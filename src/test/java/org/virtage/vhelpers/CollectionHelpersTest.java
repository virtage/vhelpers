/*******************************************************************************
 * Copyright (c) 2013 Virtage Software, Libor Jelinek.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Virtage Software, Libor Jelinek - initial API and implementation
 *******************************************************************************/
package org.virtage.vhelpers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import org.junit.Test;
import org.virtage.vhelpers.JavaHelpers;

/**
 * @author libor
 * 
 */
@SuppressWarnings("static-method")
public class CollectionHelpersTest {

	@Test(expected=NullPointerException.class)
	public void dictionaryToMapNulls() {
		CollectionHelpers.dictionaryToMap(null, null);
	}
	
	@Test(expected=NullPointerException.class)
	public void dictionaryToMapNullSource() {
		CollectionHelpers.dictionaryToMap(null, new HashMap<>());
	}

	@Test(expected=NullPointerException.class)
	public void dictionaryToMapNullTarget() {
		CollectionHelpers.dictionaryToMap(new Hashtable<>(), null);
	}
	
	@Test
	public void dictionaryToMap() {
		Dictionary<String, String> dict = new Hashtable<>();
		dict.put("favoriteSong", "Hello Dolly");
		
		Map<String, String> actual = CollectionHelpers.dictionaryToMap(dict, new HashMap<String, String>());
		
		HashMap<String, String> expected = new HashMap<>();
		expected.put("favoriteSong", "Hello Dolly");
		
		assertEquals(expected, actual);
	}
	
	
	@Test
	public void dictionaryToString() {
		assertEquals("Null argument", "",
				CollectionHelpers.dictionaryToString(null));
		assertEquals("Empty dictionary", "{}",
				CollectionHelpers.dictionaryToString(new Hashtable<>()));
		
		Properties props = new Properties();
		props.put("favoriteSong", "Hello Dolly");
		props.put("favoriteHero", "Superman");
		
		String expected = "{favoriteHero=Superman, favoriteSong=Hello Dolly}"; 
		String actual = CollectionHelpers.dictionaryToString(props);
		
		assertEquals("conversion", expected, actual);
	}
	
	@Test
	public void mapToString() {
		assertEquals("Null argument", "",
				CollectionHelpers.mapToString(null));
		assertEquals("Empty map", "{}",
				CollectionHelpers.mapToString(new Hashtable<>()));
		
		
		HashMap<String, String> map = new HashMap<>();
		map.put("favoriteSong", "Hello Dolly");
		map.put("favoriteHero", "Superman");
		
		String expected = "{favoriteHero=Superman, favoriteSong=Hello Dolly}"; 
		String actual = CollectionHelpers.mapToString(map);
		
		assertEquals("conversion", expected, actual);
	}	
}
