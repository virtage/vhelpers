package org.virtage.vhelpers;

import static org.junit.Assert.assertEquals;
import static org.virtage.vhelpers.StringHelpers.unAccent;

import org.junit.Test;

public class StringHelpersTest {
	
	@Test
	public void testUnAccent() {
		assertEquals("AAAAAACEEEEIIIINOOOOOUUUUY", unAccent("ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ"));
		assertEquals("aaaaaaceeeeiiiinooooouuuuyy", unAccent("àáâãäåçèéêëìíîïñòóôõöùúûüýÿ"));
		
		// Czech http://cs.wikipedia.org/wiki/Abeceda
		assertEquals("ACDEEINORSTUUYZ", unAccent("ÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ"));
		assertEquals("acdeeinorstuuy", unAccent("áčďéěíňóřšťúůý"));
		
		// Slovak http://cs.wikipedia.org/wiki/Slovensk%C3%A1_abeceda
		assertEquals("ACDELLNOORSTUYZ", unAccent("ÄČĎÉĹĽŇÓÔŔŠŤÚÝŽ"));
		assertEquals("aacdllnoorstuyz", unAccent("áäčďĺľňóôŕšťúýž"));
	}
	
	/**
	 * Tests whether not-ASCII characters looking like accented but actually distinct are preserved.
	 * Examples are math symbols: Ð != D, Ø != O.
	 */
	@Test
	public void unAccentKeepNonAsciiNotAccentedChars() {
		 assertEquals("ÐØø", unAccent("ÐØø"));
	}
	
	@Test
	public void unAccentNullArgument() {
		 assertEquals("", unAccent(null));
	}
	
}
